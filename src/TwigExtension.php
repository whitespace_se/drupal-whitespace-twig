<?php
namespace Drupal\whitespace_twig;

use ArrayAccess;
use Collator;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\RenderableInterface;
use Drupal\Core\Template\Attribute;
use Traversable;
use Twig_Error_Runtime;
use Twig_Extension;

class TwigExtension extends Twig_Extension
{
  protected $collator;

  public function __construct(...$args)
  {
    if (class_exists('Collator')) {
      $this->collator = new Collator('sv_SE');
    }
  }

  public function getFunctions()
  {
    return [
      new \Twig_SimpleFunction('attributes', array($this, 'attributes')),
      new \Twig_SimpleFunction('icon', array($this, 'icon')),
    ];
  }

  public function getFilters()
  {
    return [
      new \Twig_SimpleFilter('link_attributes', array($this, 'linkAttributes')),
      new \Twig_SimpleFilter('link_text', array($this, 'linkText')),
      new \Twig_SimpleFilter('pick', array($this, 'pick')),
      new \Twig_SimpleFilter('unique', array($this, 'unique')),
      new \Twig_SimpleFilter('uniq', array($this, 'unique')),
      new \Twig_SimpleFilter('sort', array($this, 'sort')),
    ];
  }

  public function attributes($attributes = array())
  {
    return new Attribute($attributes ?? []);
  }

  public function icon($icon, $size = null, $attributes = null)
  {
    if (is_array($size)) {
      $attributes = $size;
      $size = null;
    }
    return [
      '#theme' => 'icon',
      '#icon' => $icon,
      '#size' => $size,
      '#attributes' => $attributes,
    ];
  }

  public function linkAttributes($element)
  {
    if ($element instanceof RenderableInterface) {
      $element = $element->toRenderable();
    }

    // Taken from Drupal\Core\Render\Element\Link::preRenderLink()
    $element += array('#options' => array());
    if (isset($element['#attributes'])) {
      $element['#options'] += array('attributes' => array());
      $element['#options']['attributes'] += $element['#attributes'];
    }
    $options = NestedArray::mergeDeep(
      $element['#url']->getOptions(),
      $element['#options']
    );

    $attrs = new Attribute($options['attributes'] ?? []);
    return $attrs->setAttribute('href', $element['#url']->toString());
  }

  public function linkText($element)
  {
    if ($element instanceof RenderableInterface) {
      $element = $element->toRenderable();
    }
    return $element['#title'];
  }

  public function pick($element, ...$args)
  {
    if ($element instanceof ArrayAccess) {
      $filtered_element = clone $element;
    } else {
      $filtered_element = $element;
    }
    foreach (array_keys($filtered_element) as $key) {
      if (!in_array($key, $args)) {
        unset($filtered_element[$key]);
      }
    }
    return $filtered_element;
  }

  public function unique($array)
  {
    if ($array instanceof Traversable) {
      $array = iterator_to_array($array);
    } elseif (!is_array($array)) {
      throw new Twig_Error_Runtime(
        sprintf(
          'The unique filter only works with arrays or "Traversable", got "%s".',
          gettype($array)
        )
      );
    }
    return array_unique($array);
  }

  public function sort($array)
  {
    if ($array instanceof Traversable) {
      $array = iterator_to_array($array);
    } elseif (!is_array($array)) {
      throw new Twig_Error_Runtime(
        sprintf(
          'The sort filter only works with arrays or "Traversable", got "%s".',
          gettype($array)
        )
      );
    }
    if (isset($this->collator)) {
      $this->collator->asort($array);
    } else {
      asort($array);
    }
    return $array;
  }
}
